<?php

namespace App\Http\Controllers;

use App\Address;
use App\Country;
use App\Developer;
use App\Http\Requests\IndexRealEstateRequest;
use App\Http\Requests\StoreRealEstateRequest;
use App\RealEstate;
use Illuminate\Support\Facades\DB;

class RealEstateController extends Controller
{
    public function index(IndexRealEstateRequest $request)
    {
        $input = $request->all();

        $realEstates = RealEstate::with(['developer', 'address', 'address.country']);

        if (!empty($input['country'])) {
            $realEstates->whereHas('address', function($query) use ($input) {
                $query->where('country_id', $input['country']);
            });
        }

        if (!empty($input['max_price'])) {
            $realEstates->where('price', '<', $input['max_price']);
        }

        if (isset($input['is_on_sale'])) {
            $realEstates->where('is_on_sale', $input['is_on_sale']);
        }

        return $realEstates->get();
    }

    public function show($id)
    {
        return RealEstate::with(['developer', 'address', 'address.country'])->findOrFail($id);
    }

    public function store(StoreRealEstateRequest $request)
    {
        $input = $request->all();

        $developer = Developer::find($input['developer']);
        $country = Country::find($input['country']);

        DB::beginTransaction();

        $address = new Address();
        $address->city = $input['city'];
        $address->country()->associate($country);
        $address->save();

        $realEstate = new RealEstate();
        $realEstate->name = $input['name'];
        $realEstate->price = $input['price'];
        $realEstate->is_on_sale = $input['is_on_sale'];
        $realEstate->developer()->associate($developer);
        $realEstate->address()->associate($address);
        $realEstate->save();

        DB::commit();

        return response()->json($realEstate, 201);
    }
}
