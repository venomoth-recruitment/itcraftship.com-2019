<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRealEstateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'developer' => 'required|integer|min:1|exists:developers,id',
            'city' => 'required|string|max:255',
            'country' => 'required|integer|min:1|exists:countries,id',
            'price' => 'required|numeric|between:0.01,999999.99',
            'is_on_sale' => 'required|boolean',
        ];
    }
}
