<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RealEstate extends Model
{
    public function developer()
    {
        return $this->belongsTo(Developer::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }
}
