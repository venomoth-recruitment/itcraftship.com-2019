import VueRouter from 'vue-router';
import RealEstateIndex from './components/RealEstate/Index.vue';
import RealEstateCreate from './components/RealEstate/Create.vue';
import RealEstateShow from './components/RealEstate/Show.vue';

let routes = [
    { path: '/', name: 'root', component: RealEstateIndex },
    { path: '/create', name: 'create', component: RealEstateCreate },
    { path: '/show/:id', name: 'show', component: RealEstateShow }
];

export default new VueRouter({
    routes
});
