import Vue from 'vue';
import VueRouter from 'vue-router';
import router from './routes';
import App from './components/App.vue';

require('./bootstrap');

Vue.use(VueRouter);

window.axios = require('axios/dist/axios');
window.Vue = require('vue');

new Vue({
    el: '#vue-root',
    router,
    template: '<app/>',
    components: {App}
});
