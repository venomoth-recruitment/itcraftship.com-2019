<?php

namespace Tests\Feature;

use App\Country;
use App\RealEstate;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RealEstateIndexTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @dataProvider parametersValuesValidationDataProvider
     */
    public function testParametersValueValidation($parameters, $expectedStatus)
    {
        $response = $this->json('GET', '/api/real_estates', $parameters);
        $response->assertStatus($expectedStatus);
    }

    public function parametersValuesValidationDataProvider()
    {
        //[country] valid
        yield '[country] not present' => [[], 200];

        //[country] invalid
        yield '[country] zero' => [['country' => 0], 422];
        yield '[country] negative' => [['country' => -1], 422];
        yield '[country] float' => [['country' => 1.1], 422];
        yield '[country] string' => [['country' => 'a'], 422];

        //[max_price] valid
        yield '[max_price] not present' => [[], 200];
        yield '[max_price] minimum' => [['max_price' => 0.01], 200];
        yield '[max_price] maximum' => [['max_price' => 999999.99], 200];

        //[max_price] invalid
        yield '[max_price] zero' => [['max_price' => 0], 422];
        yield '[max_price] negative' => [['max_price' => -1], 422];
        yield '[max_price] string' => [['max_price' => 'a'], 422];

        //[is_on_sale] valid
        yield '[is_on_sale] not present' => [[], 200];
        yield '[is_on_sale] true #1' => [['is_on_sale' => true], 200];
        yield '[is_on_sale] true #2' => [['is_on_sale' => 1], 200];
        yield '[is_on_sale] true #3' => [['is_on_sale' => '1'], 200];
        yield '[is_on_sale] false #1' => [['is_on_sale' => false], 200];
        yield '[is_on_sale] false #2' => [['is_on_sale' => 0], 200];
        yield '[is_on_sale] false #3' => [['is_on_sale' => '0'], 200];

        //[is_on_sale] invalid
        yield '[is_on_sale] float' => [['is_on_sale' => 1.1], 422];
        yield '[is_on_sale] negative' => [['is_on_sale' => -1], 422];
        yield '[is_on_sale] string' => [['is_on_sale' => 'a'], 422];
    }

    public function testCountryExistenceValidation()
    {
        $country = factory(Country::class)->create();

        $response = $this->json('GET', '/api/real_estates', ['country' => $country->id]);
        $response->assertStatus(200);

        $response = $this->json('GET', '/api/real_estates', ['country' => $country->id+1]);
        $response->assertStatus(422);
    }

    public function testCountryFilter()
    {
        factory(RealEstate::class, 10)->create();

        $firstRealEstate = RealEstate::with(['address'])->find(1);

        $expectedRealEstates = RealEstate::with(['developer', 'address', 'address.country'])
            ->whereHas('address', function($query) use ($firstRealEstate) {
                $query->where('country_id', $firstRealEstate->address->country_id);
            })
            ->get();

        $response = $this->json('GET', '/api/real_estates', [
            'country' => $firstRealEstate->address->country_id
        ]);
        $response->assertStatus(200);
        $response->assertJson($expectedRealEstates->toArray());
    }

    public function testPriceFilter()
    {
        factory(RealEstate::class, 10)->create();

        $firstRealEstate = RealEstate::find(1);

        $expectedRealEstates = RealEstate::with(['developer', 'address', 'address.country'])
            ->where('price', '<', $firstRealEstate->price)
            ->get();

        $response = $this->json('GET', '/api/real_estates', [
            'max_price' => $firstRealEstate->price
        ]);
        $response->assertStatus(200);
        $response->assertJson($expectedRealEstates->toArray());
    }

    public function testIsOnSaleFilter()
    {
        factory(RealEstate::class, 10)->create();

        $firstRealEstate = RealEstate::find(1);

        $expectedRealEstates = RealEstate::with(['developer', 'address', 'address.country'])
            ->where('is_on_sale', $firstRealEstate->is_on_sale)
            ->get();

        $response = $this->json('GET', '/api/real_estates', [
            'is_on_sale' => $firstRealEstate->is_on_sale
        ]);
        $response->assertStatus(200);
        $response->assertJson($expectedRealEstates->toArray());
    }
}
