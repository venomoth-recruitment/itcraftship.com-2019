<?php

use App\Address;
use App\Country;
use Faker\Factory;
use Illuminate\Database\Seeder;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $countries = Country::all('id');

        foreach ($countries as $country) {
            Address::create([
                'city' => $faker->city,
                'country_id' => $country->id
            ]);
        }
    }
}
