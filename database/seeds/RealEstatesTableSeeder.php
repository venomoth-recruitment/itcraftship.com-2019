<?php

use App\Address;
use App\Developer;
use App\RealEstate;
use Faker\Factory;
use Illuminate\Database\Seeder;

class RealEstatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $developers = Developer::all('id');
        $addresses = Address::all('id');

        foreach ($developers as $developer) {
            foreach ($addresses as $address) {
                RealEstate::create([
                    'developer_id' => $developer->id,
                    'address_id' => $address->id,
                    'name' => $faker->catchPhrase,
                    'price' => $faker->randomFloat(2, 100000, 1000000),
                    'is_on_sale' => $faker->boolean,
                ]);
            }
        }
    }
}
