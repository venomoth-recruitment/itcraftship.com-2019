<?php

use App\Country;
use Faker\Factory;
use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Country::create([
                'name' => $faker->country
            ]);
        }
    }
}
