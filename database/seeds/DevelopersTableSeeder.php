<?php

use App\Developer;
use Faker\Factory;
use Illuminate\Database\Seeder;

class DevelopersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Developer::create([
                'name' => $faker->company
            ]);
        }
    }
}
