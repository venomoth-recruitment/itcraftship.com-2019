<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\RealEstate::class, function (Faker\Generator $faker) use ($factory) {
    return [
        'developer_id' => $factory->create(App\Developer::class),
        'address_id' => $factory->create(App\Address::class),
        'name' => $faker->catchPhrase,
        'price' => $faker->randomFloat(2, 100000, 1000000),
        'is_on_sale' => $faker->boolean
    ];
});
