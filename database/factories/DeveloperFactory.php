<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Developer::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company
    ];
});
