<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Address::class, function (Faker\Generator $faker) use ($factory) {
    return [
        'country_id' => $factory->create(App\Country::class),
        'city' => $faker->city
    ];
});
