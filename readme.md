# Real Estate Challenge

Repository contains solution for given challenge. Original description of task can be found in [readme-original.md](./readme-original.md) file.

All listed requirements were implemented including bonuses.

Postman collection for testing API endpoints is in file [real-estate.postman_collection.json](./real-estate.postman_collection.json).

## Database schema

![Database schema](database_schema.png)

## Installation

### Docker

[Installation for mac](https://docs.docker.com/docker-for-mac/install/)

[Installation for windows](https://docs.docker.com/docker-for-windows/install/)

### Run Docker containers

```shell
docker-compose up -d
```

### Install Composer packages
```shell
docker-compose exec app composer install
```

### Run migrations
```shell
docker-compose exec app php artisan migrate
```

### [Optional] Seed database with fake data
```shell
docker-compose exec app php artisan db:seed
```

### Set correct permissions if api router is not working
```shell
docker-compose exec app chmod -R 755 storage
```

## Run application

Go to [http://127.0.0.1:8080](http://127.0.0.1:8080) in your browser.

## Run tests
```shell
docker-compose exec app vendor/bin/phpunit
```
